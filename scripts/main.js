var scoresUrl = "",
    settingsUrl = "",
    contentUrl = "http://rovinggolf.com/admin/content",
    pageSize = 10,
    currentPage = 0,
    totalPages = 0,
    scores,
    settings,
    marque_width,
    sponsorDuration,
    teamPhotoDuration,
    marqueRate = 20,
    paginationIntervalId,
    scoreIntervalId;

function getParam(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}

function applyScoreSettings() {
    var showLeaderboard = scores.Leaderboard['@showLeaderboard'] == 'Y' ? true : false,
        showGross = scores.Leaderboard['@showGross'] == 'Y' ? true : false,
        isEventComplete = scores.Leaderboard['@eventComplete'] == 'Y' ? true : false;

    if (showGross) {
        $('table th:nth-child(4)').text('Score (Gross)');
    } else {
        $('table th:nth-child(4)').text('Score (Net)');
    }

    if (showLeaderboard && isEventComplete)
        $('.leaderboard .status').text('Event Complete');
    else if (showLeaderboard)
        $('.leaderboard .status').text('Live Scores');
    else if (!showLeaderboard) {
        $('.leaderboard .status').text('Delayed Scores');
    } else {
        $('.leaderboard .status').text('Event not started');
    }

    if (isEventComplete) {
        clearInterval(scoreIntervalId);
    }
}

function applyGeneralSetting() {
    lbPageChangeRate = parseInt(settings.LeaderboardSettings.LBPageChangeRate);
    sponsorDuration = parseInt(settings.LeaderboardSettings.MainSponsorDuration) * 1000;
    teamPhotoDuration = parseInt(settings.LeaderboardSettings.TeamPhotoRate) * 1000;
    marqueRate = parseFloat(settings.LeaderboardSettings.TickerBarRate) * 100; //animation lasts 1 min
    //start refresh
    scoreIntervalId = setInterval(fetchScores, lbPageChangeRate * 1000);
}

function cyclePages() {
    if (currentPage < totalPages) {
        $('.next > a').trigger('click');
    } else {
        $('.pagination li:not(.prev,.next):first a').trigger('click');
    }
}

function fetchScores() {
    $.ajax(scoresUrl, {
        'dataType': 'xml'
    })
        .done(function (data) {
            scores = xml2json(data);
            scores = scores.replace('\n', '');
            scores = scores.replace('{undefined"', '{"');
            scores = JSON.parse(scores);
            $('body').trigger('SCORES_FETCHED');
        });
}

function fetchSettings(urls) {
    if (urls != undefined && urls.scoresUrl != undefined && urls.settingsUrl != undefined) {
        scoresUrl = urls.scoresUrl;
        settingsUrl = urls.settingsUrl;

        $.ajax(settingsUrl, {
            'dataType': 'xml'
        })
            .done(function (data) {
                settings = xml2json(data);
                settings = settings.replace('\n', '');
                settings = settings.replace('{undefined"', '{"');
                settings = JSON.parse(settings);
                $('body').trigger('SETTINGS_FETCHED');
            });
    }
}

function changePage(e) {
    e.preventDefault();
    selectedPage = $(this).attr('data-page');

    if (selectedPage == 'prev') {
        if (currentPage == 0) return;
        selectedPage = currentPage - 1;
    } else if (selectedPage == 'next') {
        if (currentPage + 1 > totalPages) return;
        selectedPage = currentPage + 1;
    }

    $('table tr:nth-child(n+2)').hide();
    start = pageSize * selectedPage + 2;
    for (i = start; i < start + pageSize; i++) {
        $('table tr:nth-child(' + i + ')').show();
    }
    currentPage = parseInt(selectedPage); //set global
    $(".active").removeClass("active");
    $(".pagination a[data-page=" + currentPage + "]").parent().addClass("active");
}

function buildPagination() {
    var entries = scores.Leaderboard.LBEntry,
        prevHtml = "<li class=\"prev\"><a data-page=\"prev\" href=\"#\">&nbsp;</a></li>",
        nextHtml = "<li class=\"next\"><a data-page=\"next\" href=\"#\">&nbsp;</a></li>";
    $('.pagination ul').append(prevHtml);

    noOfPage = entries.length / pageSize;
    totalPages = parseInt(noOfPage); //set global

    for (i = 0; i < noOfPage; i++) {
        pageHtml = "<li><a data-page=\"" + i + "\" href=\"#\">" + (i + 1) + "</a></li>";
        $('.pagination ul').append(pageHtml);
    }
    $('.pagination ul').append(nextHtml);
    $('table tr:nth-child(n+' + (pageSize + 2) + ')').hide();
    $('.pagination a').click(changePage);
    $('.pagination a[data-page=0]').click();
}

function buildScores() {

    var entries = [],
		showLeaderboard = scores.Leaderboard['@showLeaderboard'] == 'Y' ? true : false,
		showPlayerNames = (settings.LeaderboardSettings.DisplayPlayerNames != null && settings.LeaderboardSettings.DisplayPlayerNames == "Y") ? true : false,
        showGross = scores.Leaderboard['@showGross'] == 'Y' ? true : false;
    if (scores.Leaderboard.LBEntry.length == undefined) {
        entries.push(scores.Leaderboard.LBEntry)
    } else {
        entries = scores.Leaderboard.LBEntry
    }
    for (i = 0; i < entries.length; i++) {
        cls = i % 2 == 0 ? '' : 'alt';
        if (showPlayerNames) {
            teamDetails = entries[i].Name + "<br/><span>" + entries[i].PlayerNames + "</span>";
        } else {
            teamDetails = entries[i].Name;
        }
        if (showLeaderboard) {
            //for table
            html = "<tr class=" + cls + "><td>" + entries[i].Position + "</td><td>" + teamDetails + "</td><td>" + entries[i].HolesThru + "</td><td>";
            if (showGross) {
                html += entries[i].GrossScore + "</td></tr>";
            } else {
                html += entries[i].NetScore + "</td></tr>";
            }

            //for marque
            if (showGross) {
                myScore = entries[i].GrossScore;
            } else {
                myScore = entries[i].NetScore;
            }
            html1 = "<li><strong>" + entries[i].Position + "</strong> " + entries[i].Name + " <span>" + myScore + "<span></li>"
        } else {
            //for table
            html = "<tr class=" + cls + "><td>" + "</td><td>" + teamDetails + "</td><td>" + entries[i].HolesThru + "</td><td>" + "</td></tr>";

            //for marque
            html1 = "<li>" + entries[i].Name + "</li>"
        }
        $('table tr:last').after(html);
        if ($('.marque li:last').length == 0) {
            $('.marque ul').append(html1);
        } else {
            $('.marque li:last').after(html1);
        }
    }
}

function buildTeamPhoto(photoObj) {
    imgUrl = photoObj.imageURL;
    imgUrl = imgUrl.replace('./content', contentUrl)
    html = "<div class=\"team\"><img src=\"" + imgUrl + "\"></div>";
    if ($('.team-photos .team:last').length == 0) {
        $('.team-photos').append(html);
    } else {
        $('.team-photos .team:last').after(html);
    }
}

function buildTeamPhotos() {
    var teamPhotos = settings.LeaderboardSettings.TeamPhotos;
    if (teamPhotos != null) {
        if (teamPhotos.Photo.length != undefined) {
            for (i = 0; i < teamPhotos.Photo.length; i++) {
                buildTeamPhoto(teamPhotos.Photo[i]);
            }
        } else {
            buildTeamPhoto(teamPhotos.Photo);
        }
    }
}

function buildSponsorImage(imgObj) {
    imgUrl = imgObj.imageURL;
    imgUrl = imgUrl.replace('./content', contentUrl)
    html = "<div class=\"sponsor\"><img src=\"" + imgUrl + "\"></div>";
    if ($('.sponsor-images .sponsor:last').length == 0) {
        $('.sponsor-images').append(html);
    } else {
        $('.sponsor-images .sponsor:last').after(html);
    }
}

function buildSponsorImages() {
    var sponsorImages = settings.LeaderboardSettings.SponsorImages;
    if (sponsorImages != null) {
        if (sponsorImages.Image.length != undefined) {
            for (i = 0; i < sponsorImages.Image.length; i++) {
                buildSponsorImage(sponsorImages.Image[i]);
            }
        } else {
            buildSponsorImage(sponsorImages.Image);
        }
    }
}

function buildMarque() {
    var entries = scores.Leaderboard.LBEntry,
        mWidth = entries.length * 2;
    $('.marque li').each(function () {
        mWidth += $(this).outerWidth();
    })
    $('.marque ul').width(mWidth);
    marque_width = mWidth;
}

function animateMarque() {
    $('.marque ul').supremate({
        left: (marque_width - $('.marque').width()) * -1
    }, marqueRate, "linear", function () {
        $(this).css('left', 50);
        animateMarque();
    });

    /*   $('.marque ul').animate({
           left: (marque_width - $('.marque').width()) * -1
       }, 200000, "linear", );*/
}

function animatePages() {
    paginationIntervalId = setInterval(cyclePages, lbPageChangeRate * 1000);
}

function animateSponsorImages(currentObj) {
    if (currentObj == undefined) {
        currentObj = $('.sponsor-images .sponsor:first');
    }

    if (currentObj.prev() != undefined) {
        previousObj = currentObj.prev();
        previousObj.fadeOut(function () {
            currentObj.fadeIn().css('display', 'table-cell');
        });
    } else {
        currentObj.fadeIn().css('display', 'table-cell');;
    }

    setTimeout(function () {
        if (currentObj.next().length == 0) {
            animateSponsorImages();
            currentObj.fadeOut();
        } else {
            animateSponsorImages(currentObj.next());
        }
    }, sponsorDuration);
}

function animateTeamImages(currentObj) {
    if (currentObj == undefined) {
        currentObj = $('.team-photos .team:first');
    }

    if (currentObj.prev() != undefined) {
        previousObj = currentObj.prev();
        previousObj.fadeOut(function () {
            currentObj.fadeIn().css('display', 'table-cell');
        });

    } else {
        currentObj.fadeIn().css('display', 'table-cell');
    }

    setTimeout(function () {
        if (currentObj.next().length == 0) {
            currentObj.fadeOut();
            animateTeamImages();
        } else {
            animateTeamImages(currentObj.next());
        }
    }, teamPhotoDuration);
}

function resetScores() {
    $('table tr:nth-child(n+2)').remove();
    $('.marque ul li').remove();
}

function resetPagination() {
    clearInterval(paginationIntervalId);
    $('.pagination ul li').remove();
}

function settingsSuccess() {
    applyGeneralSetting();
    $('h1')[0].innerHTML = settings.LeaderboardSettings.EventName;
    buildTeamPhotos();
    buildSponsorImages();
    if (settings.LeaderboardSettings.SponsorImages.Image.length > 1) {
        animateSponsorImages();
    } else {
        //dont animate single image
        currentObj = $('.sponsor-images .sponsor:first');
        currentObj.fadeIn().css('display', 'table-cell');
    }
    animateTeamImages();

    fetchScores();
}

function scoresSuccess() {
    resetScores();
    resetPagination();

    buildScores();
    buildPagination();
    buildMarque();
    animateMarque();

    animatePages();
    applyScoreSettings();
}
